angular
    .module('ordersapp')
    .controller('NewOrderCtrl', function ($scope, OrdersService, $state) {
        $scope.order = null;

        $scope.saveOrder = function(){
            OrdersService.addOrder($scope.order);
            $state.go('orders.list');
        };
    });