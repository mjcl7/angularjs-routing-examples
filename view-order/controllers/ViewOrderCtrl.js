angular
    .module('ordersapp')
    .controller('ViewOrderCtrl', function ($scope, OrdersService, $stateParams){
        $scope.order = OrdersService.getOrder($stateParams.idx);
    });